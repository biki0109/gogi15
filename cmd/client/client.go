package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogi15/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type Gogi15Client struct {
	pb.Gogi15Client
}

func NewGogi15Client(address string) *Gogi15Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogi15 service", l.Error(err))
	}

	c := pb.NewGogi15Client(conn)

	return &Gogi15Client{c}
}
